package cz.cvut.fel.ts1;


public class Svorcjak {
    public long factorial (long n){
        if (n == 0){
            long result = 1;
            return result;
        }
        else if(n < 0){
            long result = n;
            return  result;
        }
        else {
            long result = n;
            for (long i = 1; i < n; i++) {
                result = result * (n - i);
            }
            return result;
        }
    }
}
