package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SvorcjakTest {
    @Test
    public void factorial_nIs1_returns1() {
        Svorcjak svorcjak = new Svorcjak();
        long expected = 1;

        long actual = svorcjak.factorial(1);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIs0_returns1() {
        Svorcjak svorcjak = new Svorcjak();
        long expected = 1;

        long actual = svorcjak.factorial(0);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial_nIs4_returns24() {
        Svorcjak svorcjak = new Svorcjak();
        long expected = 24;

        long actual = svorcjak.factorial(4);

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void factorial_nIsminus4_returnsMinus1() {
        Svorcjak svorcjak = new Svorcjak();
        long expected = -1;

        long actual = svorcjak.factorial(-4);

        Assertions.assertEquals(expected, actual);
    }
}
